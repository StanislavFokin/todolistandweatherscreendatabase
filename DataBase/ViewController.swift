//
//  ViewController.swift
//  DataBase
//
//  Created by Станислав Фокин on 23/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstNameTextfield: UITextField!
        
        @IBOutlet weak var lastNameTextfield: UITextField!
        
        let persistance = Persistance.shared

        override func viewDidLoad() {
            super.viewDidLoad()
            firstNameTextfield.text = persistance.firstName
            lastNameTextfield.text = persistance.lastName
            //persistance.trashWeatherRealm()
        }
        @IBAction func firstName(_ sender: UITextField) {
            persistance.firstName = firstNameTextfield.text
        }
        
        @IBAction func lastName(_ sender: UITextField) {
            persistance.lastName = lastNameTextfield.text
        }
    }

