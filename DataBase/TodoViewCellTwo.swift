//
//  TodoViewCellTwo.swift
//  DataBase
//
//  Created by Станислав Фокин on 28/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import UIKit

class TodoViewCellTwo: UITableViewCell {
    
    let coreDataManager = CoreDataManager.shared
    var reminder = [Reminder] ()
    
    @IBOutlet weak var labelReminder: UILabel!
}
