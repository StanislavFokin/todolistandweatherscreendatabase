//
//  Persistance.swift
//  DateBaseApp
//
//  Created by Станислав Фокин on 20/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import CoreData
import RealmSwift
import Alamofire

class RealmObject: Object{
    @objc dynamic var reminder = ""
}

class Day: Object {
    @objc dynamic var day = ""
    @objc dynamic var id = 0
    override static func primaryKey() -> String? {
        return "id"
    }
}

class Temp: Object {
    @objc dynamic var temp = 0
    @objc dynamic var id = 0
    override static func primaryKey() -> String? {
        return "id"
    }
}


class Persistance{
    
    let coreDataManager = CoreDataManager.shared
    var reminder = [Reminder] ()
    var realmObject: Results<RealmObject>?
    var dayObject: Results<Day>?
    var tempObject: Results<Temp>?
    var todoList = [String] ()
    var day = Day()
    var temp = Temp()
    
    static let shared = Persistance()
    
    private let realm = try! Realm()
    
    private let firstNameKey = "Persistance.firstNameKey"
    private let lastNameKey = "Persistance.lastNameKey"

    var firstName: String? {
        set { UserDefaults.standard.set(newValue, forKey: firstNameKey) }
        get { return UserDefaults.standard.string(forKey: firstNameKey) }
    }
    
    var lastName: String? {
        set { UserDefaults.standard.set(newValue, forKey: lastNameKey)}
        get { return UserDefaults.standard.string(forKey: lastNameKey) }
    }
    
    func addReminderRealm (nameReminder: String){
        let reminder = RealmObject()
        reminder.reminder = nameReminder
        try! realm.write {
            realm.add(reminder)
        }
    }
    
    func addWeatherRealm(allDay: String, allTemp: Int, weatherID: Int) {
        //func loadWeatherAlamofire(completion: @escaping ([Weather]) -> Void){
        let day = Day()
        let temp = Temp()
        //let temp = WeatherObject()
        day.day = allDay
        day.id = weatherID
        temp.temp = allTemp
        temp.id = weatherID
        //if day.day != "", temp.temp != 0 {
        try! realm.write {
            //realm.add(day, update: .modified)
            //realm.add(temp, update: .modified)
            //var person = realm.create(Person.self, value: ["Jane", 27])
            realm.add(day, update: .modified)
            realm.add(temp, update: .modified)
            //realm.add(day)
            //realm.add(temp)
        }
    }
    
    func loadDataRealm() {
        let allObject = realm.objects(RealmObject.self)
        realmObject = allObject
    }
    
    func loadWeatherObject() {
        print("loadWeatherObject")
        let dayObject = realm.objects(Day.self)
        let tempObject = realm.objects(Temp.self)
        self.dayObject = dayObject
        self.tempObject = tempObject
        printRealm()
        //trashWeatherRealm()
    }
    
    func updateWeatherObject(day: String, temp: Int){
        let dayObject = realm.objects(Day.self)
        let tempObject = realm.objects(Temp.self)
        try! realm.write{
            for updateDay in dayObject{
                updateDay.day = day
                //tempObject.temp = temp
            }
            for updateTemp in tempObject{
                updateTemp.temp = temp
                //tempObject.temp = temp
            }
        }
    }
    
    func trashWeatherRealm() {
        let dayObject = realm.objects(Day.self)
        let tempObject = realm.objects(Temp.self)
        try! realm.write{
            realm.delete(dayObject)
            realm.delete(tempObject)
        }
    }
    
    func trashRealmReminder(at index: Int){
        let trashReminder = realmObject![index]
        try! realm.write {
            realm.delete(trashReminder)
        }
    }
    
    func trashRealmLastReminder(){
        guard let trashReminder = realmObject!.last else { return print("nil") }
        try! realm.write {
            realm.delete(trashReminder)
        }
    }
    
    func addReminder (nameReminder: String){
        let reminder = Reminder()
        reminder.reminder = nameReminder
        coreDataManager.save()
    }
    
    func trashReminder(at index: Int) {
        let trashReminder = reminder[index]
        coreDataManager.delete(trashReminder)
    }
    
    func trashLastReminder() {
        let trashReminder = reminder.last
        if let s = trashReminder {
            coreDataManager.delete(s)
        }
    }
    
    func loadData() {
        let reminder = coreDataManager.fetch(Reminder.self)
        self.reminder = reminder
        printReminder()
    }
    
    func printReminder() {
        reminder.forEach({print("Это reminder - \(String(describing: $0.reminder))")})
    }
    
    func printRealm(){
        tempObject?.forEach({print("Это temp - \(String(describing: $0.temp))")})
        dayObject?.forEach({print("Это day - \(String(describing: $0.day))")})
    }
}

