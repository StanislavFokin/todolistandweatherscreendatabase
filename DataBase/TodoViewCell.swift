//
//  TodoViewCell.swift
//  DateBaseApp
//
//  Created by Станислав Фокин on 22/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import UIKit

class TodoViewCell: UITableViewCell {
    
    let coreDataManager = CoreDataManager.shared
    var reminder = [Reminder] ()
    
    @IBOutlet weak var labelReminder: UILabel!
}
