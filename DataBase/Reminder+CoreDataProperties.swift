//
//  Reminder+CoreDataProperties.swift
//  DataBase
//
//  Created by Станислав Фокин on 23/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//
//

import Foundation
import CoreData


extension Reminder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Reminder> {
        return NSFetchRequest<Reminder>(entityName: "Reminder")
    }

    @NSManaged public var reminder: String?

}
