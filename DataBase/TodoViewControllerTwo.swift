//
//  TodoViewControllerTwo.swift
//  DataBase
//
//  Created by Станислав Фокин on 28/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import UIKit

class TodoViewControllerTwo: UIViewController {
    
    let persistance = Persistance.shared
    
    @IBOutlet weak var tableViewList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        persistance.loadDataRealm()
    }
    @IBAction func trashLastReminder(_ sender: UIBarButtonItem) {
        persistance.trashRealmLastReminder()
        //persistance.loadDataRealm()
        tableViewList.reloadData()
    }
    
    @IBAction func addReminder(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Create new reminder", message: nil, preferredStyle: .alert)
        
        alertController.addTextField{(textField) in
        }
        let alertButtonOne = UIAlertAction(title: "Cancel", style: .default) {(alert) in
        }
        
        let alertButtonTwo = UIAlertAction(title: "Create", style: .default) {(alert) in
            let newReminder = alertController.textFields![0].text
            self.persistance.addReminderRealm(nameReminder: newReminder!)
            //self.persistance.loadDataRealm()
            self.tableViewList.reloadData()
        }
        alertController.addAction(alertButtonOne)
        alertController.addAction(alertButtonTwo)
        
        present(alertController, animated: true, completion: nil)
    }
}
extension TodoViewControllerTwo: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistance.realmObject!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoViewCellTwo") as! TodoViewCellTwo
        let model = persistance.realmObject![indexPath.row].reminder
        cell.labelReminder.text = model
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            persistance.trashRealmReminder(at: indexPath.row)
            //self.persistance.loadDataRealm()
            self.tableViewList.reloadData()
        }
        else if editingStyle == .insert {
        }
    }
}
