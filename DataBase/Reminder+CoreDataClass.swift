//
//  Reminder+CoreDataClass.swift
//  DataBase
//
//  Created by Станислав Фокин on 23/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Reminder)
public class Reminder: NSManagedObject {
    convenience init() {
        self.init(entity: CoreDataManager.shared.entityForName(entityName: "Reminder"), insertInto: CoreDataManager.shared.context)
    }
}
