//
//  WeatherLoad.swift
//  DataBase
//
//  Created by Станислав Фокин on 01/03/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class WeatherLoad {
    
    let persistance = Persistance.shared
    var index = 0
    
    func loadWeatherAlamofire(completion: @escaping ([Weather]) -> Void){
    AF.request("https://api.openweathermap.org/data/2.5/forecast?id=524894&units=metric&lang=ru&APPID=9b60ecad911dc5b0193dfec9474b5b7a").responseJSON
            { response in
                if let wheatherObject = response.value,
                    let jsonDict = wheatherObject as? NSDictionary {
                    //print(jsonDict)
                    var weatherData: [Weather] = []
                    for (_, data) in jsonDict where data is [NSDictionary] {
                        if let dateNext = data as? [NSDictionary] {
                            for weather in dateNext {
                                if let weatherDate = Weather(data: weather){
                                    weatherData.append(weatherDate)
                                }
                            }
                        }
                    }
                //print(weatherData)
                DispatchQueue.main.async {
                    completion(weatherData)
                }
            }
        }
    }
    func loadWeatherAlamofireTwo(completion: @escaping (Results<Day>?, Results<Temp>?) -> Void){
    AF.request("https://api.openweathermap.org/data/2.5/forecast?id=524894&units=metric&lang=ru&APPID=9b60ecad911dc5b0193dfec9474b5b7a").responseJSON
            { response in
                if let wheatherObject = response.value,
                    let jsonDict = wheatherObject as? NSDictionary {
                    for (_, data) in jsonDict where data is [NSDictionary] {
                        if let dateNext = data as? [NSDictionary] {
                            for weather in dateNext {
                                if let weatherDate = Weather(data: weather){
                                    self.index += 1
                                    self.persistance.addWeatherRealm(allDay: weatherDate.day, allTemp: weatherDate.temp, weatherID: self.index)
                                    print("loadWeatherAlamofireTwo - \(self.index)")
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        completion(self.persistance.dayObject, self.persistance.tempObject)
                    }
                }
            }
    }
}
