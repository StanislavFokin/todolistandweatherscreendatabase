//
//  WeatherCell.swift
//  DataBase
//
//  Created by Станислав Фокин on 01/03/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import UIKit
import Foundation

class WeatherCell: UITableViewCell{
    
    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var tempLabel: UILabel!
    
}
