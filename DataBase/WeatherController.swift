//
//  WeatherController.swift
//  DataBase
//
//  Created by Станислав Фокин on 01/03/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import RealmSwift

class WeatherController: UIViewController {
    
    @IBOutlet var cityLabel: UILabel!
    
    @IBOutlet var tempLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    let persistance = Persistance.shared
    
    //var weatherData: [Weather] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("awakeFromNib")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        do {
            let realm = try Realm()
            let day = realm.objects(Day.self)
            let temp = realm.objects(Temp.self)
            self.persistance.dayObject = day
            self.persistance.tempObject = temp
            if let s = self.persistance.tempObject?.first {
                self.cityLabel.text = "Moscow"
                self.tempLabel.text = "\(s.temp)℃"
            }
        }
        catch {
             print(error)
        }
        WeatherLoad().loadWeatherAlamofireTwo { (day, temp) in
            self.persistance.dayObject = day
            self.persistance.tempObject = temp
            self.tempLabel.text = "\(self.persistance.tempObject![0].temp)℃"
            self.cityLabel.text = "Moscow"
            self.tableView.reloadData()
        }
    }
}

extension WeatherController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistance.dayObject!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell") as! WeatherCell
        let day = persistance.dayObject![indexPath.row]
        let temp = persistance.tempObject![indexPath.row]
        //tempLabel.text = "\(temp.temp)℃"
        cell.dayLabel.text = day.day
        cell.tempLabel.text = "\(temp.temp)"
        return cell
    }
}

