//
//  TodoViewController.swift
//  DateBaseApp
//
//  Created by Станислав Фокин on 22/02/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import Foundation
import UIKit

class TodoViewController: UIViewController {
    
    let persistance = Persistance.shared
    
    @IBOutlet weak var tableViewList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        persistance.loadData()
    }
    @IBAction func trashLastReminder(_ sender: UIBarButtonItem) {
        persistance.trashLastReminder()
        persistance.loadData()
        tableViewList.reloadData()
    }
    
    @IBAction func addReminder(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Create new reminder", message: nil, preferredStyle: .alert)
        
        alertController.addTextField{(textField) in
        }
        let alertButtonOne = UIAlertAction(title: "Cancel", style: .default) {(alert) in
        }
        
        let alertButtonTwo = UIAlertAction(title: "Create", style: .default) {(alert) in
            let newReminder = alertController.textFields![0].text
            self.persistance.addReminder(nameReminder: newReminder!)
            self.persistance.loadData()
            self.tableViewList.reloadData()
        }
        alertController.addAction(alertButtonOne)
        alertController.addAction(alertButtonTwo)
        
        present(alertController, animated: true, completion: nil)
    }
}
extension TodoViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persistance.reminder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoViewCell") as! TodoViewCell
        let model = persistance.reminder[indexPath.row].reminder
        cell.labelReminder.text = model
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            persistance.trashReminder(at: indexPath.row)
            self.persistance.loadData()
            self.tableViewList.reloadData()
        }
        else if editingStyle == .insert {
        }
    }
}
