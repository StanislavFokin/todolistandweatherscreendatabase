//
//  Weather.swift
//  DataBase
//
//  Created by Станислав Фокин on 01/03/2020.
//  Copyright © 2020 Stanislav Fokin. All rights reserved.
//

import UIKit
import Foundation

class Weather {
    
    let persistance = Persistance.shared
   
    var day: String
    var temp: Int
    
    init?(data: NSDictionary){
        var dayDate = String()
        var temp = Int()
        if let date = data["dt"] as? Double {
            let day = Date(timeIntervalSince1970: date)
            let weekDay = day.dayOfWeek()!
            dayDate = weekDay
        } else {return nil}
        if let tempCatalog = data["main"] as? NSDictionary {
            if let s = tempCatalog["temp"] as? Double {
                temp = Int(s)
            }
        } else {return nil}
        self.day = dayDate
        self.temp = temp
    }
}
extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, HH:mm:ss"
        return dateFormatter.string(from: self).capitalized
    }
}
